(function(APP){
    APP.utils = (function(){
        function module(){
            var exports = {
                dashToCamelCase: function (str) {
                    var camelCased = "";
                    
                    if(typeof str === "string"){
                        camelCased =  str.replace(/-([a-z])/g, function (g) {
                            return g[1].toUpperCase();
                        });
                    }
                    
                    return camelCased;
                },
                timeSince: function (date) {
                    var seconds = Math.floor((new Date() - new Date(date)) / 1000);
                    var interval = Math.floor(seconds / 31536000);

                    if (interval > 1) {
                        return interval + " years";
                    }
                    interval = Math.floor(seconds / 2592000);
                    if (interval > 1) {
                        return interval + " months";
                    }
                    interval = Math.floor(seconds / 86400);
                    if (interval > 1) {
                        return interval + " days";
                    }
                    interval = Math.floor(seconds / 3600);
                    if (interval > 1) {
                        return interval + " hours";
                    }
                    interval = Math.floor(seconds / 60);
                    if (interval > 1) {
                        return interval + " minutes";
                    }
                    return Math.floor(seconds) + " seconds";
                },
                getScript: function(url, callback) {
                    var head = document.getElementsByTagName("head")[0];
                    var script = document.createElement("script");
                    script.src = url;

                    // Handle Script loading
                    {
                        var done = false;

                        // Attach handlers for all browsers
                        script.onload = script.onreadystatechange = function(){
                            if ( !done && (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") ) {
                                done = true;
                                if (callback){
                                    callback();
                                }

                                // Handle memory leak in IE
                                script.onload = script.onreadystatechange = null;
                            }
                        };
                    }

                    head.appendChild(script);

                    // We handle everything using the script element injection
                    return undefined;
                },
                toNumber: function(numberString){
                	var num = 0;
                	
                	if(!isNaN(numberString)){
                		num = Number(numberString);
                	}
                	
                	return num;
                }
            };
            
            return exports;
        }
        
        return new module();
    })();
})(window.APP || {});