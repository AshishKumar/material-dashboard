(function(APP){
    APP.config = (function(){
        function module(){
            var exports = {
                app: {
                    name: "PMO Portal : POC",
                    author: "Ashish Kumar",
                    version: "1.0.0",
                    date: "Aug 10, 2016"
                },
                urls: {
                    restRoot: {
                        api: "/api/",
                        local: "/api/"
                    },
                    jsRoot: "/js/"
                },
                plugins: {}
            };
            
            return exports;
        }
        
        return new module();
    })();
})(window.APP = window.APP || {});