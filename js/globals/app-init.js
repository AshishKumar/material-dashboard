(function(APP){
    $(function(){
        
        var $modules = $("[data-module]");
        
        console.log("APP initlizing...");
        
        $modules.each(function(){
            var module = $(this).data("module");
            var jsPath = APP.config.urls.jsRoot + "modules/" + module  + ".js";
            
            APP.utils.getScript(jsPath, function(){
                APP[APP.utils.dashToCamelCase(module)].init();
            });
        })
        
    });
})(window.APP = window.APP || {});