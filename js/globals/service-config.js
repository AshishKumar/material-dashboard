(function(APP){
    APP.serviceConfig = (function(){
        function module(){
            var exports = {
                global: {
                    getProducts: "products.json"
                },
                dashboard: {
                }
            };
            
            return exports;
        }
        
        return new module();
    })();
})(window.APP || {});