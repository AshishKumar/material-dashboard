$(function(){
    $(".button-collapse").sideNav();
    $('select').material_select();
    
    var chartData = {
        risks: [
            ['High', 30],
            ['Medium', 120],
            ['Low', 40]
        ],
        issues: [
            ['High', 50],
            ['Medium', 10],
            ['Low', 90]
        ],
        pendings: [
            ['Decisions', 30],
            ['Actions', 130],
            ['Change Requests', 50]
        ],
        budget: [
            ['dates', '2016-01-15', '2016-02-15', '2016-03-15', '2016-04-15', '2016-05-15', '2016-06-15', '2016-07-15', '2016-08-15', '2016-09-15', '2016-10-15', '2016-11-15', '2016-12-15'],
            ['Planed', 35000, 25000, 35000, 45000, 48000, 52000, 32000, 43000, 56000, 72000, 34000, 25000],
            ['Actual', 32000, 27000, 39000, 52000, 34000, 32000]
        ]
    };
    
    var charts = (function(){
        var risks = c3.generate({
            bindto: '#risks',
            data: {
                columns: chartData.risks,
                colors: {
                    High: '#d40000',
                    Medium: '#ff9400',
                    Low: '#00d48b'
                },
                type : 'donut'
            },
            tooltip: {
                format: {
                    value: function (value, ratio, id) {
                        var format = d3.format(',');
                        return format(value);
                    }
                }
            },
            donut: {
                title: "All Risks"
            }
        });
        
        var issues = c3.generate({
            bindto: '#issues',
            data: {
                columns: chartData.issues,
                colors: {
                    High: '#d40000',
                    Medium: '#ff9400',
                    Low: '#00d48b'
                },
                type : 'donut'
            },
            tooltip: {
                format: {
                    value: function (value, ratio, id) {
                        var format = d3.format(',');
                        return format(value);
                    }
                }
            },
            donut: {
                title: "All Issues"
            }
        });
        
        var pendings = c3.generate({
            bindto: '#pendings',
            padding: {
                right: 10
            },
            data: {
                columns: chartData.pendings,
                type: 'bar'
            },
            bar: {
                width: {
                    ratio: 0.5 // this makes bar width 50% of length between ticks
                }
            },
            tooltip: {
                grouped: false,
                contents: function(d, defaultTitleFormat, defaultValueFormat, color){
                    defaultTitleFormat = function () {
                        return "";
                    }
                    return c3.chart.internal.fn.getTooltipContent.apply(this, arguments);
                }
            }
        });
        
        var budget = c3.generate({
            bindto: '#budget',
            padding: {
                right: 10
            },
            data: {
                x: 'dates',
                xFormat: '%Y-%m-%d',
                columns: chartData.budget,
                colors: {
                    Planed: '#ff9400',
                    Actual: '#00d48b'
                }
            },
            zoom: {
                enabled: true
            },
            axis: {
                x: {
                    type: 'timeseries',
                    tick: {
                        format: "%b-%d"
                    }
                },
                y: {
                    tick: {
                        format: d3.format('$,')
                    }
                }
            },
            tooltip: {
                format: {
                    title: function(timeStamp){
                        var parseDate = d3.time.format("%b %d, %Y");
                        return parseDate(timeStamp);
                    }
                }
            }
        });
        
        return {
            risks: risks,
            issues: issues,
            pendings: pendings,
            budget: budget
        };
    })();
    
    
    
    //
    $("form").on("submit", function(e){
        $(this).closest(".card-reveal").velocity({translateY: 0}, {
            duration: 225,
            queue: false,
            easing: 'easeInOutQuad',
            complete: function() {
                var chartName = $(this).prev().attr("id");
                
                function getRandNum(min, max){
                    return Math.ceil(Math.abs(Math.random() * max - Math.random() * min));
                }
                
                // Reload chart
                if(chartName !== "budget"){
                    chartData[chartName][0][1] = getRandNum(80, 20);
                    chartData[chartName][1][1] = getRandNum(100, 45);
                    chartData[chartName][2][1] = getRandNum(75, 20);
                    
                    charts[chartName].load({
                        columns: chartData[chartName]
                    });
                }
                
                $(this).css({ display: 'none'});
                Materialize.toast("<em>Filters applied !!</em>", 3e3 );
            }
        });
        e.preventDefault();
    });
});