(function(APP) {
	"use strict";

	APP.header = (function() {
		function module() {

			function bindEvents() {
			}

			function init() {
                $(".button-collapse").sideNav();
                $('select').material_select();

				// Init Event Binding
				bindEvents();
			}

			return {
				init : init
			};
		}

		return new module();
	})();
})(window.APP || {});